<?php

namespace BL\CloudflareBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('bl_cloudflare');
        $rootNode = $treeBuilder->getRootNode();

        $builder = $rootNode->children();
        $builder->scalarNode('email');
        $builder->scalarNode('api_key');

        return $treeBuilder;
    }
}
