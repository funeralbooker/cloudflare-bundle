<?php

namespace BL\CloudflareBundle\Command;

use BL\CloudflareBundle\Service\Cloudflare;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CachePurgeEverythingCommand extends Command
{
    /**
     * @var Cloudflare
     */
    private $cloudflare;

    /**
     * @param Cloudflare $cloudflare
     */
    public function __construct(Cloudflare $cloudflare)
    {
        $this->cloudflare = $cloudflare;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('cloudflare:cache:purge-everything')
            ->setDescription('Purge whole cloudflare cache')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $zonesEndpoint = $this->cloudflare->getZones();
        foreach ($zonesEndpoint->listZones()->result as $zone) {
            $output->writeln(sprintf('Cache purge for %s status: %s', $zone->name, $zonesEndpoint->cachePurgeEverything($zone->id) === true ? "successful" : "failed"));
        }

        $output->writeln('Done!');

	return 0;
    }
}
