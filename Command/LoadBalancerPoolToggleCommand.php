<?php

namespace BL\CloudflareBundle\Command;

use BL\CloudflareBundle\Service\Cloudflare;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class LoadBalancerPoolToggleCommand extends Command
{
    /**
     * @var Cloudflare
     */
    private $cloudflare;

    /**
     * @param Cloudflare $cloudflare
     */
    public function __construct(Cloudflare $cloudflare)
    {
        $this->cloudflare = $cloudflare;

        parent::__construct();
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('cloudflare:load-balancer-pool:toggle')
            ->setDescription('Enabled or disabled load balancer pool with given name')
            ->addArgument(
                'name',
                InputArgument::REQUIRED,
                'Load Balancer Pool name'
            )
            ->addArgument(
                'enabled',
                InputArgument::REQUIRED,
                'Flag if Load Balancer Pool should be enabled or disabled (1 - enabled, 0 - disabled)'
            )
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $enabled = $input->getArgument('enabled');

        if (1 !== (int)$enabled && 0 !== (int)$enabled) {
            throw new \Exception('Invalid argument "enabled". It should be or 1 or 0.');
        }

        $loadBalancerPoolEndpoint = $this->cloudflare->getLoadBalancerPools();
        $loadBalancerPool = $loadBalancerPoolEndpoint->findLoadBalancerPoolByName($name);

        if ($loadBalancerPool !== null) {
            $method = 1 === (int)$enabled ? 'enableLoadBalancerPool': 'disableLoadBalancerPool';

            $options = [
                'monitor' => $loadBalancerPool->monitor,
                'check_regions' => $loadBalancerPool->check_regions,
                'description' => $loadBalancerPool->description,
                'notification_email' => $loadBalancerPool->notification_email,
            ];

            $loadBalancerPoolEndpoint->$method($loadBalancerPool->id, $loadBalancerPool->name, $loadBalancerPool->origins, $options);
            $output->writeln(sprintf('Load Balancer Pool with name %s has been %s!', $name, 1 === (int)$enabled ? 'enabled' : 'disabled'));
        }

	return 0;
    }
}
