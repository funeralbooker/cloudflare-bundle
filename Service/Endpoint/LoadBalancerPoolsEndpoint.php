<?php

namespace BL\CloudflareBundle\Service\Endpoint;

use Cloudflare\API\Adapter\Adapter;
use Cloudflare\API\Endpoints\API;

class LoadBalancerPoolsEndpoint implements API
{
    /**
     * @var Adapter
     */
    private $adapter;

    /**
     * @param Adapter $adapter
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @return \stdClass
     */
    public function listLoadBalancerPools(): \stdClass
    {
        $user = $this->adapter->get('user/load_balancers/pools');
        $body = json_decode($user->getBody());

        return (object)['result' => $body->result];
    }

    /**
     * @param string $name
     * @return null|\stdClass
     */
    public function findLoadBalancerPoolByName(string $name): ?\stdClass
    {
        $list = $this->listLoadBalancerPools();

        foreach ($list->result as $loadBalancerPool) {
            if ($loadBalancerPool->name === $name) {
                return $loadBalancerPool;
            }
        }

        return null;
    }

    /**
     * @param string $poolId
     * @param string $name
     * @param array  $origins
     * @param array  $options
     *
     * @return \stdClass
     */
    public function disableLoadBalancerPool(string $poolId, string $name, array $origins, array $options = []): \stdClass
    {
        $options['enabled'] = false;

        return $this->updateLoadBalancerPool($poolId, $name, $origins, $options);
    }

    /**
     * @param string $poolId
     * @param string $name
     * @param array  $origins
     * @param array  $options
     *
     * @return \stdClass
     */
    public function enableLoadBalancerPool(string $poolId, string $name, array $origins, array $options = []): \stdClass
    {
        $options['enabled'] = true;

        return $this->updateLoadBalancerPool($poolId, $name, $origins, $options);
    }

    /**
     * @param string $poolId
     * @param string $name
     * @param array $origins
     * @param array $options
     *
     * @return \stdClass
     */
    public function updateLoadBalancerPool(string $poolId, string $name, array $origins, array $options): \stdClass
    {
        $options['name'] = $name;
        $options['origins'] = $origins;

        $user = $this->adapter->put(sprintf('user/load_balancers/pools/%s', $poolId), $options);
        $body = json_decode($user->getBody());

        return (object)['result' => $body->result];
    }
}
