<?php

namespace BL\CloudflareBundle\Service;

use BL\CloudflareBundle\Service\Endpoint\LoadBalancerPoolsEndpoint;
use Cloudflare\API\Adapter\Guzzle;
use Cloudflare\API\Endpoints;

class Cloudflare
{
    /**
     * @var Guzzle
     */
    private $client;

    /**
     * @var \Cloudflare\API\Endpoints\AccessRules
     */
    private $accessRules;

    /**
     * @var \Cloudflare\API\Endpoints\DNS
     */
    private $dns;

    /**
     * @var \Cloudflare\API\Endpoints\IPs
     */
    private $ips;

    /**
     * @var \Cloudflare\API\Endpoints\PageRules
     */
    private $pageRules;

    /**
     * @var \Cloudflare\API\Endpoints\Railgun
     */
    private $railgun;

    /**
     * @var \Cloudflare\API\Endpoints\UARules
     */
    private $uaRules;

    /**
     * @var \Cloudflare\API\Endpoints\User
     */
    private $user;

    /**
     * @var \Cloudflare\API\Endpoints\WAF
     */
    private $waf;

    /**
     * @var \Cloudflare\API\Endpoints\Zones
     */
    private $zones;

    /**
     * @var \Cloudflare\API\Endpoints\ZoneLockDown
     */
    private $zoneLockDown;

    /**
     * @var LoadBalancerPoolsEndpoint
     */
    private $loadBalancerPools;

    /**
     * @param Guzzle $client
     */
    public function __construct(Guzzle $client)
    {
        $this->client = $client;
        $this->accessRules = $this->get('AccessRules');
        $this->dns = $this->get('DNS');
        $this->ips = $this->get('IPs');
        $this->pageRules = $this->get('PageRules');
        $this->railgun = $this->get('Railgun');
        $this->uaRules = $this->get('UARules');
        $this->user = $this->get('User');
        $this->waf = $this->get('WAF');
        $this->zones = $this->get('Zones');
        $this->zoneLockDown = $this->get('ZoneLockdown');
        $this->loadBalancerPools = $this->get('LoadBalancerPools');
    }

    /**
     * @return Guzzle
     */
    public function getClient(): Guzzle
    {
        return $this->client;
    }

    /**
     * @return Endpoints\AccessRules
     */
    public function getAccessRules(): Endpoints\AccessRules
    {
        return $this->accessRules;
    }

    /**
     * @return Endpoints\DNS
     */
    public function getDns(): Endpoints\DNS
    {
        return $this->dns;
    }

    /**
     * @return Endpoints\IPs
     */
    public function getIps(): Endpoints\IPs
    {
        return $this->ips;
    }

    /**
     * @return Endpoints\PageRules
     */
    public function getPageRules(): Endpoints\PageRules
    {
        return $this->pageRules;
    }

    /**
     * @return Endpoints\Railgun
     */
    public function getRailgun(): Endpoints\Railgun
    {
        return $this->railgun;
    }

    /**
     * @return Endpoints\UARules
     */
    public function getUaRules(): Endpoints\UARules
    {
        return $this->uaRules;
    }

    /**
     * @return Endpoints\User
     */
    public function getUser(): Endpoints\User
    {
        return $this->user;
    }

    /**
     * @return Endpoints\WAF
     */
    public function getWaf(): Endpoints\WAF
    {
        return $this->waf;
    }

    /**
     * @return Endpoints\Zones
     */
    public function getZones(): Endpoints\Zones
    {
        return $this->zones;
    }

    /**
     * @return Endpoints\ZoneLockDown
     */
    public function getZonesLockDown(): Endpoints\ZoneLockDown
    {
        return $this->zoneLockDown;
    }

    /**
     * @return LoadBalancerPoolsEndpoint
     */
    public function getLoadBalancerPools(): LoadBalancerPoolsEndpoint
    {
        return $this->loadBalancerPools;
    }

    /**
     * @param $endpoint
     *
     * @return Endpoints\API
     */
    private function get($endpoint): Endpoints\API
    {
        $className = sprintf('\Cloudflare\API\Endpoints\%s', $endpoint);

        if (!class_exists($className)) {
            $className = sprintf('BL\CloudflareBundle\Service\Endpoint\%sEndpoint', $endpoint);
        }

        return new $className($this->client);
    }
}
