<?php

namespace BL\CloudflareBundle\Service\Factory;

use BL\CloudflareBundle\Service\Cloudflare;
use Cloudflare\API\Auth\APIKey;
use Cloudflare\API\Adapter\Guzzle;

class CloudflareFactory
{
    /**
     * @param string $email
     * @param string $apiKey
     *
     * @return Cloudflare
     */
    public function create(string $email, string $apiKey): Cloudflare
    {
        $cloudflareApiKey = new APIKey($email, $apiKey);
        $client = new Guzzle($cloudflareApiKey);

        return new Cloudflare($client);
    }
}
